# API "Мои заказы"

### Разработать метод GET /my_orders

```json
{
    "active" : 12980,
    "credit" : 10900,
    "orders" : [
        {
            "id" : "12345",
            "number" : "№331272619-005",
            "date" : "12.04.2021",
            "sum" : "12 980 ₽",
            "status" : {
                "id" : "1",
                "name" : "Собирается"
            },
            "id" : "54321",
            "number" : "№331272619-004",
            "date" : "14.04.2021",
            "sum" : "10 900 ₽",
            "status" : {
                "id" : "2",
                "name" : "В пути"
            },
        }
    ]
}
```
